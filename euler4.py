def search():
	for i in range(100, 1000):
		for j in range(100, 1000):
			p = str(i * j)
			if p == p[::-1]: yield int(p)

print(max(iter(search())))
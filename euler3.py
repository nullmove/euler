def highest_primefactor(n):
	highest = 0
	for i in range(2, int(n**0.5) + 1):
		if n%i == 0:
			p = max(map(lambda x: x if is_prime(x) else 0, (i, n//i)))
			if p > highest: highest = p
	return highest

def is_prime(n):
	for i in range(2, int(n**0.5) + 1):
		if n%i == 0: return False
	return True

print(highest_primefactor(600851475143))
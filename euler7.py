from itertools import count, islice

def is_prime(n):
	for i in range(2, int(n**0.5) + 1):
		if n%i == 0: return False
	return True

print(next(islice(filter(is_prime, iter(count(3, 2))), 9999, 10000)))

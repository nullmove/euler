def fibonacci():
	a, b, c = 0, 1, 0
	while c < 4000000:
		c = a + b
		yield c
		a, b = b, c

print(sum(filter(lambda x: x%2 == 0, iter(fibonacci()))))
